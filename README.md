# Keepass2AndroidSharePlugin #

Send credentials to external applications using one-time pads

### What is this repository for? ###

* To share keepass credentials with external applications

### How do I get set up? ###

* To test it: ./gradlew assemblyDebug
* Will generate the build apk in app/build/outputs/apk/app-debug.apk

### Contribution guidelines ###

* Writing tests (There are none so far)
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin