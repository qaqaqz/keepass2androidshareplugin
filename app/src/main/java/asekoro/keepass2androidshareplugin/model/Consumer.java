package asekoro.keepass2androidshareplugin.model;

/**
 * Created by alber on 31/12/2016.
 */
public interface Consumer<T> {
    void consume(T input);
}
