package asekoro.keepass2androidshareplugin.model;

import java.io.Serializable;

/**
 * Created by alber on 31/12/2016.
 */

public class QrCodeData implements Serializable {
    private final String id;
    private final String url;
    private final byte[] oneTimePad;

    public QrCodeData(String id, String url, byte[] oneTimePad){
        this.id = id;
        this.url = url;
        this.oneTimePad = oneTimePad;
    }

    public byte[] getOneTimePad() {
        return oneTimePad;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }
}
