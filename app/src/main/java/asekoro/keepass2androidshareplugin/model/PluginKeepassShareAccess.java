package asekoro.keepass2androidshareplugin.model;

import java.util.ArrayList;

import keepass2android.pluginsdk.Strings;

/**
 * Created by alber on 01/01/2017.
 *
 * Definition of the Keepass2Android plugin
 */

public class PluginKeepassShareAccess
        extends keepass2android.pluginsdk.PluginAccessBroadcastReceiver
{

    @Override
    public ArrayList<String> getScopes() {
        ArrayList<String> scopes = new ArrayList<String>();
        scopes.add(Strings.SCOPE_DATABASE_ACTIONS);
        scopes.add(Strings.SCOPE_CURRENT_ENTRY);
        scopes.add(Strings.SCOPE_QUERY_CREDENTIALS);
        return scopes;

    }

}