package asekoro.keepass2androidshareplugin.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import asekoro.keepass2androidshareplugin.R;

/**
 * Created by albert on 19/08/17.
 */

public class FragmentUtils {
    public static Fragment getFirstVisibleFragment(FragmentManager fragmentManager){
        for(Fragment fragment : fragmentManager.getFragments()){
            if(fragment!=null && fragment.isVisible()){
                return fragment;
            }
        }
        return null;
    }

    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment){
        try {
            int previousIndex=-1;
            Fragment visibleFragment = getFirstVisibleFragment(fragmentManager);
            if(visibleFragment!=null){
                previousIndex = getIndexFragment(visibleFragment);
            }
            int newIndex = FragmentUtils.getIndexFragment(fragment);
            if (visibleFragment!=null && visibleFragment.getClass() == fragment.getClass()) {
                Log.d("MainActivity", "Clicked on the same fragment");
                return;
            }
            boolean left = newIndex < previousIndex;
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            if (left) {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            } else {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            }
            fragmentTransaction.replace(R.id.content, fragment);
            fragmentTransaction.addToBackStack(null);
            if(fragmentManager.getBackStackEntryCount()>0) {
                fragmentManager.popBackStack();
            }
            fragmentTransaction.commit();
        }catch(Throwable t){
            Log.e("MainActivity", "Error", t);
        }
    }

    public static final int RC_HANDLE_CAMERA_PERM = 2;
    public static boolean ensureCameraPermision(Fragment fragment){

        int rc = ActivityCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            final String[] permissions = new String[]{Manifest.permission.CAMERA};
            fragment.requestPermissions(permissions, RC_HANDLE_CAMERA_PERM);
            return false;
        }
    }

    public static void toast(Fragment fragment, String text){
        showToast(Snackbar.make(fragment.getView(), text, 2000));
    }

    public static void toast(Fragment fragment, int textReference){
        showToast(Snackbar.make(fragment.getView(), textReference, 2000));
    }

    private static void showToast(final Snackbar snackbar){
        snackbar.setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }


    public static Bundle getFragmentBundle(Fragment fragment){
        Bundle bundle = fragment.getArguments();
        if(bundle==null){
            bundle = new Bundle();
            fragment.setArguments(bundle);
        }
        return bundle;
    }

    public static void setIndexFragment(Fragment fragment, int index){
        getFragmentBundle(fragment).putInt("index",index);
    }

    public static int getIndexFragment(Fragment fragment){
        return getFragmentBundle(fragment).getInt("index");
    }

}
