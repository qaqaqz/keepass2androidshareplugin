package asekoro.keepass2androidshareplugin.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import asekoro.keepass2androidshareplugin.R;
import asekoro.keepass2androidshareplugin.camera.BaseBarcodeReaderFragment;
import asekoro.keepass2androidshareplugin.camera.CameraSourcePreview;
import asekoro.keepass2androidshareplugin.model.QrCodeData;
import asekoro.keepass2androidshareplugin.util.FragmentUtils;

public class ScanFragment extends BaseBarcodeReaderFragment {
    CameraSourcePreview cameraSourcePreview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.scan_view, container, false);
        cameraSourcePreview = view.findViewById(R.id.preview);
        return view;
    }

    @Override
    protected CameraSourcePreview getCameraSourcePreview() {
        return cameraSourcePreview;
    }

    @Override
    protected boolean qrCodeDetected(QrCodeData qrCodeData) {
        FragmentUtils.toast(this, "QRCode received from "+ qrCodeData.getUrl());
        Fragment fragment = RequestKeyFragment.newInstance(qrCodeData);
        FragmentUtils.setIndexFragment(fragment, 2);
        FragmentUtils.replaceFragment( getFragmentManager(),fragment);
        return false;
    }
}