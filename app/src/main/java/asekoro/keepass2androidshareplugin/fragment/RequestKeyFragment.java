package asekoro.keepass2androidshareplugin.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import asekoro.keepass2androidshareplugin.R;
import asekoro.keepass2androidshareplugin.connector.KeyServer;
import asekoro.keepass2androidshareplugin.model.QrCodeData;
import asekoro.keepass2androidshareplugin.util.FragmentUtils;
import keepass2android.pluginsdk.KeepassDefs;
import keepass2android.pluginsdk.Kp2aControl;

import static android.app.Activity.RESULT_OK;

public class RequestKeyFragment extends Fragment {
    private Button mButtonScan;
    private static final String QR_CODE_KEY="qrCodeData";
    private Button mButtonScan2;
    private TextView mTextRequestId;

    public static RequestKeyFragment newInstance(QrCodeData qrCodeData){
        RequestKeyFragment requestKeyFragment = new RequestKeyFragment();
        FragmentUtils.getFragmentBundle(requestKeyFragment).putSerializable(QR_CODE_KEY, qrCodeData);
        return requestKeyFragment;
    }
    QrCodeData qrCodeData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.request_key_layout, container, false);
        mButtonScan = view.findViewById(R.id.button_request_with_domain);
        mButtonScan2 = view.findViewById(R.id.button_request_with_domain2);
        mTextRequestId = view.findViewById(R.id.textView);
        qrCodeData = null;
        try {
            qrCodeData = (QrCodeData) getArguments().getSerializable(QR_CODE_KEY);
        }catch (Throwable t){
            //Ignore, not qrCodeData found
        }
        if(qrCodeData==null){
            mButtonScan.setText("Empty qrCodeData");
        }else{
            mButtonScan.setText(qrCodeData.getUrl());
            mTextRequestId.setText("Request id: " + qrCodeData.getId());
        }

        mButtonScan.setOnClickListener(getOnClick(RequestKeyFragment.this.qrCodeData.getUrl()));
        mButtonScan2.setOnClickListener(getOnClick(""));

        return view;
    }

    private View.OnClickListener getOnClick(final String domain){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    askCredentials(domain);
            }
        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 1) //queryEntry for own package
                && (resultCode == RESULT_OK)) // ensure user granted access and selected something
        {
            HashMap<String, String> credentials = Kp2aControl.getEntryFieldsFromIntent(data);
            if (!credentials.isEmpty())
            {
                String username = credentials.get(KeepassDefs.UserNameField);
                String password = credentials.get(KeepassDefs.PasswordField);
                FragmentUtils.toast(this, "retrieved credenitals! Username="+username);
                new SendTask("https://keepassqr.herokuapp.com", username, password).execute();
            }
        }else{
            FragmentUtils.toast(this, "Authorization failed.");
        }
    }
    private void askCredentials(String domain) {
        try {
            this.startActivityForResult(
                    Kp2aControl.getQueryEntryIntent(domain),
                    1);
        } catch (ActivityNotFoundException e) {
            FragmentUtils.toast(this, "no KP2A host app found");
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    class SendTask extends AsyncTask<String, Void, String> {
        private final String host;
        private final String username;
        private final String password;

        public SendTask(String host, String username, String password){
            this.host = host;
            this.username = username;
            this.password = password;
        }

        private Exception exception;

        protected String doInBackground(String... urls) {
            try {
                new KeyServer(host).sendKey(qrCodeData,username+";"+password+";");
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            if(this.exception!=null){
                FragmentUtils.toast(RequestKeyFragment.this, "Error sending the data. " + this.exception.getMessage());
            }else{
                FragmentUtils.toast(RequestKeyFragment.this, "Data Sent");
            }
        }
    }
}