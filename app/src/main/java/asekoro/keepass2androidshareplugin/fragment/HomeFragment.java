package asekoro.keepass2androidshareplugin.fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import asekoro.keepass2androidshareplugin.R;
import asekoro.keepass2androidshareplugin.util.FragmentUtils;

public class HomeFragment extends Fragment {
    private Button mButtonScan;
    private Runnable startScanHandler;

    public void setStartScanHandler(Runnable runnable){
        startScanHandler = runnable;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.home_view, container, false);

        mButtonScan = view.findViewById(R.id.button_scan);

        mButtonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("HomeFragment","Clicked.");
                mButtonScan.setActivated(false);
                if(startScanHandler!=null){
                    startScanHandler.run();
                }
            }
        });

        return view;
    }
}