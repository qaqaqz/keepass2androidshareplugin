package asekoro.keepass2androidshareplugin;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import asekoro.keepass2androidshareplugin.fragment.HomeFragment;
import asekoro.keepass2androidshareplugin.fragment.ScanFragment;
import asekoro.keepass2androidshareplugin.fragment.SettingsFragment;
import asekoro.keepass2androidshareplugin.model.PluginKeepassShareAccess;
import asekoro.keepass2androidshareplugin.util.FragmentUtils;

import static keepass2android.pluginsdk.Strings.ACTION_RECEIVE_ACCESS;
import static keepass2android.pluginsdk.Strings.ACTION_REVOKE_ACCESS;
import static keepass2android.pluginsdk.Strings.ACTION_TRIGGER_REQUEST_ACCESS;

public class MainActivity extends AppCompatActivity {
    private Activity a = this;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    FragmentUtils.replaceFragment(getSupportFragmentManager(), getHomeFragment());
                    return true;
                case R.id.navigation_scan:
                    FragmentUtils.replaceFragment(getSupportFragmentManager(), getScanFragment());
                    return true;
                case R.id.navigation_settings:
                    FragmentUtils.replaceFragment(getSupportFragmentManager(), getSettingsFragment());
                    return true;
            }
            return false;
        }
    };

    public static ScanFragment getScanFragment(){
        ScanFragment fragment = new ScanFragment();
        FragmentUtils.setIndexFragment(fragment,2);
        return fragment;
    }

        private HomeFragment getHomeFragment(){
        HomeFragment fragment = new HomeFragment();
        FragmentUtils.setIndexFragment(fragment,1);
        fragment.setStartScanHandler(new Runnable() {
            @Override
            public void run() {
                Log.d("MainActivity","Replace fragment.");
                FragmentUtils.replaceFragment(getSupportFragmentManager(), getScanFragment());
               navigation.getMenu().getItem(1).setChecked(true);
            }
        });
        return fragment;
    }

    private SettingsFragment getSettingsFragment(){
        SettingsFragment fragment = new SettingsFragment();
        FragmentUtils.setIndexFragment(fragment,3);
        return fragment;
    }

    BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_TRIGGER_REQUEST_ACCESS);
        intentFilter.addAction(ACTION_RECEIVE_ACCESS);
        intentFilter.addAction(ACTION_REVOKE_ACCESS);
        this.getApplicationContext().registerReceiver(
                new PluginKeepassShareAccess(),
                intentFilter);

        setContentView(R.layout.activity_main);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int currentIndex = FragmentUtils.getIndexFragment(FragmentUtils.getFirstVisibleFragment(getSupportFragmentManager()));
                if(currentIndex>0 && currentIndex<=navigation.getMenu().size()) {
                    navigation.getMenu().getItem(currentIndex - 1).setChecked(true);
                }
            }
        });

        getSupportFragmentManager().beginTransaction().add(R.id.content, getHomeFragment()).commit();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                System.out.println("Exception "+ throwable.getMessage());
                throwable.printStackTrace();
            }
        });
    }
}
