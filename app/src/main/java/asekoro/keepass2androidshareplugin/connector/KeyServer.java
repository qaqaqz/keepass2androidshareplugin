package asekoro.keepass2androidshareplugin.connector;

import android.util.Base64;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import asekoro.keepass2androidshareplugin.model.QrCodeData;

/**
 * Created by albert on 18/08/17.
 */

public class KeyServer {
    private static final String URL_FORMAT = "%s/sendecho?id=%s&message=%s";

    private String host;

    /**
     *
     * @param host Example https://keepassqr.herokuapp.com/
     */
    public KeyServer(String host){
        this.host = host;
    }

    public void sendKey(QrCodeData qrCodeData, String message) throws Exception {
        byte[] encrypted = encryptString(message, qrCodeData.getOneTimePad());
        String base64ed = Base64.encodeToString(encrypted, Base64.DEFAULT);


        String url = String.format(URL_FORMAT, host, qrCodeData.getId(), URLEncoder.encode(base64ed, "UTF-8"));
        int code;
        try {
            code = HttpRequest.executeAndReturnResponseCode(url);
        } catch (IOException e) {
            throw new Exception("Could not send the key.", e);
        }
        if (code != 200) {
            throw new Exception("The key server responded with code <"+code+">");
        }
    }
    private static byte[] encryptString(String input, byte[] oneTimePad){
        byte[] encrypted = new byte[oneTimePad.length];
        input = input +"--------------------------------------------------------------------";
        byte[] inputBytes = input.getBytes(Charset.forName("UTF-8"));
        for(int i = 0; i<oneTimePad.length;i++){
            encrypted[i] = (byte)(inputBytes[i] ^ oneTimePad[i]);
        }
        return encrypted;
    }
}
