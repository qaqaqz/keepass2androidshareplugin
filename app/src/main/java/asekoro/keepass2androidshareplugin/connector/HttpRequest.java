package asekoro.keepass2androidshareplugin.connector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by albert on 18/08/17.
 */

public class HttpRequest {
    public static int executeAndReturnResponseCode(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            return urlConnection.getResponseCode();
        } finally {
            urlConnection.disconnect();
        }
    }
}
