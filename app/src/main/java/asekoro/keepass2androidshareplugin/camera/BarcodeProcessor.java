package asekoro.keepass2androidshareplugin.camera;

import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;

import asekoro.keepass2androidshareplugin.model.Consumer;
import asekoro.keepass2androidshareplugin.model.QrCodeData;

/**
 * Created by alber on 31/12/2016.
 */

public class BarcodeProcessor implements Detector.Processor<Barcode> {
    private static final String TAG = "Barcode-processor";
    private Consumer<QrCodeData> barcodeDetected;

    public BarcodeProcessor(Consumer<QrCodeData> barcodeDetected) {
        this.barcodeDetected = barcodeDetected;
    }

    @Override
    public void release() {

    }

    @Override
    public void receiveDetections(Detector.Detections<Barcode> detections) {
        try {
            SparseArray<Barcode> detectedItems = detections.getDetectedItems();
            if (detectedItems.size() > 0) {
                if (detectedItems.size() > 1) {
                    Log.d(TAG, "More than one qr code detected!");
                } else {
                    Barcode barcode = detections.getDetectedItems().valueAt(0);
                    final String value = barcode.rawValue;

                    String[] parts = value.split(";", 3);
                    String url = parts[0];
                    String id = parts[1];
                    String base64 = parts[2];
                    byte[] oneTimePad = Base64.decode(base64, Base64.DEFAULT);

                    barcodeDetected.consume(new QrCodeData(id, url, oneTimePad));
                }
            }
        }catch (Exception e){
            Log.e(TAG, "Error detecting the barcode.", e);
        }
    }
}
