package asekoro.keepass2androidshareplugin.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import asekoro.keepass2androidshareplugin.util.FragmentUtils;
import asekoro.keepass2androidshareplugin.R;
import asekoro.keepass2androidshareplugin.model.Consumer;
import asekoro.keepass2androidshareplugin.model.QrCodeData;

import static android.hardware.Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;

/**
 * Created by albert on 19/08/17.
 */

public abstract class BaseBarcodeReaderFragment extends Fragment {
    private static final String TAG = "Barcode-reader";

    private static final int RC_HANDLE_GMS = 9001;

    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;

    private boolean isActive = false;
    private String previousQR = "";

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPreview = getCameraSourcePreview();
        if (FragmentUtils.ensureCameraPermision(this)) {
            createCameraSource();
        }
    }

    protected abstract CameraSourcePreview getCameraSourcePreview();

    /**
     *
     * @param qrCodeData
     * @return If the camera should continue looking for QrCodes
     */
    protected boolean qrCodeDetected(QrCodeData qrCodeData){
        return true;
    }

    @SuppressLint("InlinedApi")
    private void createCameraSource() {
        try {
            if(mCameraSource!=null){
                return;
            }
            Context context = getContext();
            BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context)
                    .setBarcodeFormats(Barcode.QR_CODE)
                    .build();

            barcodeDetector.setProcessor(new BarcodeProcessor(new Consumer<QrCodeData>() {
                @Override
                public void consume(final QrCodeData input) {
                    try {
                        if (isActive) {
                            if (previousQR!=null &&  previousQR.equals(input.getUrl())) {
                                previousQR = null;
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(!qrCodeDetected(input)){
                                            // Pause the detector
                                            onPause();
                                        }
                                    }
                                });
                            } else {
                                previousQR = input.getUrl();
                            }
                        } else {
                            previousQR = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));

            if (!barcodeDetector.isOperational()) {
                FragmentUtils.toast(this, R.string.barcode_detector_not_ready);
                Log.w(TAG, "Detector dependencies are not yet available.");
            }

            CameraSource.Builder builder = new CameraSource.Builder(getContext(), barcodeDetector)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(160, 102)
                    .setRequestedFps(15.0f);

            // make sure that auto focus is an available option
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                builder = builder.setFocusMode(FOCUS_MODE_CONTINUOUS_PICTURE);
            }

            mCameraSource = builder
                    .build();

            startCameraSource();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        previousQR = null;
        isActive = true;
        System.out.println("Resume");
        try {
            super.onResume();
            startCameraSource();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        try {
            if (mCameraSource != null) {
                try {
                    mPreview.start(mCameraSource);
                } catch (IOException e) {
                    Log.e(TAG, "Unable to start camera source.", e);
                    mCameraSource.release();
                    mCameraSource = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        isActive = false;
        System.out.println("Pause");
        try {
            super.onPause();
            if (mPreview != null) {
                mPreview.stop();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        System.out.println("Destroy");
        try {
            super.onDestroy();
            if (mPreview != null) {
                mPreview.release();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try {
            if (requestCode != FragmentUtils.RC_HANDLE_CAMERA_PERM) {
                Log.d(TAG, "Got unexpected permission result: " + requestCode);
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                return;
            }

            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Camera permission granted - initialize the camera source");
                FragmentUtils.toast(this, "Camera permision granted.");
                createCameraSource();
                return;
            }
            FragmentUtils.toast(this, "Camera permision not granted :(.");
            Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                    " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
